package com.example.tradingappspringboot;

import com.example.tradingappspringboot.exceptions.StockDoesNotExistException;
import com.example.tradingappspringboot.exceptions.StockNotAvailableException;
import com.example.tradingappspringboot.model.Fruit;
import com.example.tradingappspringboot.model.FruitStock;
import com.example.tradingappspringboot.strategy.SellingStrategy;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class TestLifoSellingStrategy {
    @Autowired
    SellingStrategy lifoSellingStrategy;

    Fruit fruit;
    List<FruitStock> fruitStockList;

    @BeforeEach
    public void init() {
        fruit = new Fruit();
        fruit.setName("Apple");

        fruitStockList = new ArrayList<>();
        fruitStockList.add(new FruitStock(1, 50, 100, null, fruit));
        fruitStockList.add(new FruitStock(2, 50, 110, null, fruit));
        fruitStockList.add(new FruitStock(3, 50, 120, null, fruit));
    }

    @Test
    public void testSell() throws StockDoesNotExistException, StockNotAvailableException {
        int profitEarned = lifoSellingStrategy.sell("Apple", 70, 130, fruitStockList);
        Assertions.assertEquals(1900, profitEarned);
    }

    @Test
    public void testSellNotAvailableStock() {
        Assertions.assertThrows(StockNotAvailableException.class, () -> lifoSellingStrategy.sell("Apple", 170, 130, fruitStockList));
    }

    @Test
    public void testSellEmptyStock() {
        Assertions.assertThrows(StockDoesNotExistException.class, () -> lifoSellingStrategy.sell("Apple", 170, 130, null));
    }
}
