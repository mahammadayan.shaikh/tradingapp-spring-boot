package com.example.tradingappspringboot.service;

import com.example.tradingappspringboot.exceptions.StockDoesNotExistException;
import com.example.tradingappspringboot.exceptions.StockNotAvailableException;
import com.example.tradingappspringboot.model.Businessman;
import com.example.tradingappspringboot.model.Fruit;
import com.example.tradingappspringboot.model.FruitStock;
import com.example.tradingappspringboot.repository.BusinessmanRepository;
import com.example.tradingappspringboot.repository.FruitRepository;
import com.example.tradingappspringboot.repository.FruitStockRepository;
import com.example.tradingappspringboot.strategy.SellingStrategy;
import com.example.tradingappspringboot.strategy.SellingStrategyProvider;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TradingService {
    private final BusinessmanRepository businessmanRepository;
    private final FruitRepository fruitRepository;
    private final FruitStockRepository fruitStockRepository;
    private final SellingStrategy sellingStrategy;

    public TradingService(BusinessmanRepository businessmanRepository, FruitRepository fruitRepository, FruitStockRepository fruitStockRepository) {
        this.businessmanRepository = businessmanRepository;
        this.fruitRepository = fruitRepository;
        this.fruitStockRepository = fruitStockRepository;
        this.sellingStrategy = SellingStrategyProvider.getSellingStrategy("lifo");
    }

    public boolean buy(String name, int price, int quantity, int businessmanId) {
        // Retrieve businessman object who wants to buy FruitItem
        Optional<Businessman> businessmanOptional = businessmanRepository.findById(businessmanId);

        // Check if businessman exits with specified id or not
        if (businessmanOptional.isPresent()) {
            // Get Businessman Object
            Businessman businessman = businessmanOptional.get();

            Fruit fruit;

            // Check if there is already FruitItem with given name for this businessman
            // In that case we won't create new FruitItem record
            // We will simply add FruitItemStock records with foreign key of the existing FruitItem record

            Optional<Fruit> fruitItemOptional = fruitRepository.findFruitItemByNameAndBusinessmanId(name, businessmanId);
            if (fruitItemOptional.isPresent()) {
                // Retrieve existing FruitItem record
                fruit = fruitItemOptional.get();
            } else {
                // Create new FruitItem object for storing record into database
                fruit = new Fruit();
                fruit.setName(name);
                fruit.setBusinessman(businessman);
                fruitRepository.save(fruit);
            }
            // Create FruitItemStock object for storing it into database
            FruitStock stock = new FruitStock();

            // Set according values of the object
            stock.setQuantity(quantity);
            stock.setPrice(price);
            stock.setFruitItem(fruit);

            // Save record into database
            fruitStockRepository.save(stock);

            // Return Success Message
            return true;
        } else {
            // Throw exception regarding Businessman not found
            return false;
        }
    }

    public Boolean sell(String name, int price, int quantity, int businessmanId) throws StockDoesNotExistException, StockNotAvailableException {
        // Check if FruitItem is present into database or not
        Optional<Fruit> fruitItemOptional = fruitRepository.findFruitItemByNameAndBusinessmanId(name, businessmanId);

        if (fruitItemOptional.isPresent()) {
            // Retrieve FruitItem object
            Fruit fruit = fruitItemOptional.get();

            // Retrieve available stock of this FruitItem
            List<FruitStock> fruitStocks = fruitStockRepository.findFruitStocksByFruitIdOrderById(fruit.getId());

            // call sell method of SellingStrategy to perform sell operation
            // Store the profit earned by this selling
            int profitEarned = sellingStrategy.sell(name, quantity, price, fruitStocks);

            // Update the FruitItemStockList
            fruit.setFruitItemStockList(fruitStocks);

            // Add the earned profit with existing profit
            fruit.setProfit(fruit.getProfit() + profitEarned);

            // Save the updated details in database
            fruitRepository.save(fruit);
            return true;
        } else {
            return false;
        }
    }

    public int findProfit(int businessmanId) {
        // Retrieve List of FruitItem for the businessman
        List<Fruit> fruits = fruitRepository.findFruitByBusinessmanId(businessmanId);

        // Calculate the profit
        int profit = fruits.stream().collect(Collectors.summingInt(Fruit::getProfit));

        // Return profit
        return profit;
    }
}
