package com.example.tradingappspringboot.service;

import com.example.tradingappspringboot.model.Businessman;
import com.example.tradingappspringboot.repository.BusinessmanRepository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class BusinessmanService {
    // Businessman Repository to perform Db operations related to businessman table
    private final BusinessmanRepository businessmanRepository;

    public BusinessmanService(BusinessmanRepository businessmanRepository) {
        this.businessmanRepository = businessmanRepository;
    }

    public Businessman addNewBusinessman(@RequestBody Businessman businessman) {
        Businessman addedBusinessman = businessmanRepository.save(businessman);
        return addedBusinessman;
    }
}
