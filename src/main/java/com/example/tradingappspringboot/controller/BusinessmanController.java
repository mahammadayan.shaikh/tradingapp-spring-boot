package com.example.tradingappspringboot.controller;

import com.example.tradingappspringboot.model.Businessman;
import com.example.tradingappspringboot.service.BusinessmanService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/businessman")
public class BusinessmanController {
    // Service to perform business logic
    private final BusinessmanService businessmanService;

    public BusinessmanController(BusinessmanService businessmanService) {
        this.businessmanService = businessmanService;
    }

    // method to handle post request for creating new Businessman
    @PostMapping
    public Businessman addNewBusinessman(@RequestBody Businessman businessman) {
        Businessman addedBusinessman = businessmanService.addNewBusinessman(businessman);
        return addedBusinessman;
    }
}
