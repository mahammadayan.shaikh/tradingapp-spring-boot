package com.example.tradingappspringboot.controller;

import com.example.tradingappspringboot.exceptions.StockDoesNotExistException;
import com.example.tradingappspringboot.exceptions.StockNotAvailableException;
import com.example.tradingappspringboot.service.TradingService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@RestController
@RequestMapping("/businessman-trading")
public class TradingController {

    // Services to perform business logic
    private final TradingService tradingService;

    public TradingController(TradingService tradingService) {
        this.tradingService = tradingService;
    }

    // Method to handle post request of buying new FruitItem
    @PostMapping("/{businessmanId}/buyFruitItem/")
    public String buyFruitItem(@RequestBody Map<String, String> json, @PathVariable int businessmanId) {
        // Retrieve name,quantity & price of the FruitItem which is to be bought
        String name = json.get("name");
        int price = Integer.parseInt(json.get("price"));
        int quantity = Integer.parseInt(json.get("quantity"));

        // Call buy method of tradingService
        boolean isBought = tradingService.buy(name, price, quantity, businessmanId);

        // Return Response according to returned value of isBought
        if (isBought)
            return String.format("BOUGHT %d KG %s AT %d RS/KG", quantity, name, price);
        else
            throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND, "Businessman not found"
            );
    }

    @PostMapping("/{businessmanId}/sellFruitItem/")
    public String sellFruitItem(@RequestBody Map<String, String> json, @PathVariable int businessmanId) throws StockDoesNotExistException, StockNotAvailableException {
        // Parse jsom data coming from the request
        String name = json.get("name");
        int price = Integer.parseInt(json.get("price"));
        int quantity = Integer.parseInt(json.get("quantity"));

        // Call sell method of tradingService
        boolean isSold = tradingService.sell(name, price, quantity, businessmanId);

        // Return Response according to returned value of isSold
        if (isSold)
            return String.format("SOLD %d KG %s AT %d RS/KG", quantity, name, price);
        else
            // Throw exception regarding unavailability of Stock
            throw new StockDoesNotExistException("Requested item is not available in stock");
    }

    @GetMapping("/{businessmanId}/profit/")
    public int getProfit(@PathVariable int businessmanId) {
        return tradingService.findProfit(businessmanId);
    }
}
