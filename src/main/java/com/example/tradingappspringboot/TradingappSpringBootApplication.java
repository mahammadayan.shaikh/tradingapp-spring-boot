package com.example.tradingappspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradingappSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TradingappSpringBootApplication.class, args);
	}

}
