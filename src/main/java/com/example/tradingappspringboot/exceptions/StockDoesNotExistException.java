package com.example.tradingappspringboot.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class StockDoesNotExistException extends Exception {
    public StockDoesNotExistException(String message) {
        super(message);
    }
}
