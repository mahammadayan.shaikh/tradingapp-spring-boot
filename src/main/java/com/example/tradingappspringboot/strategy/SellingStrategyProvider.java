package com.example.tradingappspringboot.strategy;

public class SellingStrategyProvider {
    public static SellingStrategy getSellingStrategy(String strategy) {
        if ("lifo".equalsIgnoreCase(strategy))
            return new LifoSellingStrategy();
        return null;
    }
}
