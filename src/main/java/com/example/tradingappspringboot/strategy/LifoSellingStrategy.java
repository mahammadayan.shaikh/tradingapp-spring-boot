package com.example.tradingappspringboot.strategy;

import com.example.tradingappspringboot.exceptions.StockDoesNotExistException;
import com.example.tradingappspringboot.exceptions.StockNotAvailableException;
import com.example.tradingappspringboot.model.FruitStock;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class LifoSellingStrategy implements SellingStrategy {
    @Override
    public int sell(String fruitName, int required_quantity, int price, List<FruitStock> fruitStocks) throws StockDoesNotExistException, StockNotAvailableException {
        int profit = 0;
        // Check If we have this fruit in stock or not
        if (fruitStocks == null || fruitStocks.isEmpty()) {
            // Throw Exception regarding unavailability of stock.
            throw new StockDoesNotExistException(fruitName + " is not available in stock");
        }

        // Check if we have enough quantity of fruit in stock
        int available_quantity = fruitStocks.stream().collect(Collectors.summingInt(FruitStock::getQuantity));
        if (available_quantity < required_quantity) {
            // Throw Exception regarding unavailability of enough quantity
            throw new StockNotAvailableException(fruitName + " is under stock");
        }

        // Get Fruits from stock in LIFO manner.
        while (required_quantity > 0) {
            // Get the fruit which was bought first
            FruitStock curFruitStock = fruitStocks.get(0);
            if (required_quantity >= curFruitStock.getQuantity()) {
                // Get the entire stock of fruit as we need more quantities
                required_quantity -= curFruitStock.getQuantity();
                fruitStocks.remove(0);

                //Update the profit
                profit += ((price - curFruitStock.getPrice()) * curFruitStock.getQuantity());
            } else {
                // Decrease the quantity as we have used some quantity of it
                curFruitStock.setQuantity(curFruitStock.getQuantity() - required_quantity);

                // Update the profit
                profit += ((price - curFruitStock.getPrice()) * required_quantity);

                // Set quantity to zero as we have got the desired quantities
                required_quantity = 0;
            }
        }
        return profit;
    }
}
