package com.example.tradingappspringboot.strategy;

import com.example.tradingappspringboot.exceptions.StockDoesNotExistException;
import com.example.tradingappspringboot.exceptions.StockNotAvailableException;
import com.example.tradingappspringboot.model.FruitStock;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SellingStrategy {
    int sell(String name,int quantity,int price, List<FruitStock> fruitStocks) throws StockDoesNotExistException, StockNotAvailableException;
}
