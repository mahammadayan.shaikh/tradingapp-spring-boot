package com.example.tradingappspringboot.repository;

import com.example.tradingappspringboot.model.FruitStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FruitStockRepository extends JpaRepository<FruitStock, Integer> {
    List<FruitStock> findFruitStocksByFruitIdOrderById(int id);
}
