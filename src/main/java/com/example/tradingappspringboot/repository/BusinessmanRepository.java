package com.example.tradingappspringboot.repository;

import com.example.tradingappspringboot.model.Businessman;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BusinessmanRepository extends JpaRepository<Businessman, Integer> {
}
