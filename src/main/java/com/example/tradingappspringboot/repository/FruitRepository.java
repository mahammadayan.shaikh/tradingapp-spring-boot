package com.example.tradingappspringboot.repository;

import com.example.tradingappspringboot.model.Fruit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FruitRepository extends JpaRepository<Fruit, Integer> {
    Optional<Fruit> findFruitItemByNameAndBusinessmanId(String name, Integer businessmanId);

    List<Fruit> findFruitByBusinessmanId(Integer businessmanId);
}
