package com.example.tradingappspringboot.model;

import javax.persistence.*;
import java.util.List;

@Entity
public class Fruit {
    @Id
    @GeneratedValue
    private int id;

    // Name of fruit item
    private String name;

    // Profit earned by businessman from this fruit item
    private int profit = 0;

    // Foreign Key of Businessman Table to store which businessman bought this fruit item
    @ManyToOne
    private Businessman businessman;

    // List of entries from Child Table(FruitItemStock)
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "fruit", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<FruitStock> fruitStockList;

    // Getters - Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Businessman getBusinessman() {
        return businessman;
    }

    public void setBusinessman(Businessman businessman) {
        this.businessman = businessman;
    }

    public List<FruitStock> getFruitItemStockList() {
        return fruitStockList;
    }

    public void setFruitItemStockList(List<FruitStock> fruitStockList) {
        // clear entire list
        this.fruitStockList.clear();

        // Add all items into list
        this.fruitStockList.addAll(fruitStockList);
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    @Override
    public String toString() {
        return "FruitItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", profit=" + profit +
                ", businessman=" + businessman +
                ", fruitItemStockList=" + fruitStockList +
                '}';
    }
}
