package com.example.tradingappspringboot.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Businessman {
    @Id
    @GeneratedValue
    private int id;

    // Name of Businessman
    private String name;

    // All fruit items bought by the businessman
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "businessman", cascade = CascadeType.ALL)
    private Set<Fruit> fruits;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Fruit> getFruitItems() {
        return fruits;
    }

    public void setFruitItems(Set<Fruit> fruits) {
        this.fruits = fruits;
    }

    @Override
    public String toString() {
        return String.format("Businessman[id=%d,name=%s]", id, name);
    }
}
