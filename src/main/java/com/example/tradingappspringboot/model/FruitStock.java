package com.example.tradingappspringboot.model;

import javax.persistence.*;
import java.util.Date;

@Entity
public class FruitStock {
    @Id
    @GeneratedValue
    private int id;

    private int quantity;

    private int price;

    @Temporal(TemporalType.TIMESTAMP)
    private Date utilDate;

    @ManyToOne
    private Fruit fruit;

    public FruitStock() {
    }

    public FruitStock(int id, int quantity, int price, Date utilDate, Fruit fruit) {
        this.id = id;
        this.quantity = quantity;
        this.price = price;
        this.utilDate = utilDate;
        this.fruit = fruit;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Fruit getFruitItem() {
        return fruit;
    }

    public void setFruitItem(Fruit fruit) {
        this.fruit = fruit;
    }

    @Override
    public String toString() {
        return "FruitItemStock{" +
                "id=" + id +
                ", quantity=" + quantity +
                ", price=" + price +
                ", utilDate=" + utilDate +
                '}';
    }
}
